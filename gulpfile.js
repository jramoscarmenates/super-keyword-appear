var gulp = require('gulp')
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')

// Angularjs app Clinic Services.
gulp.task('app', function () {
    gulp.src(['templates/js/app/**/*.js'])
        .pipe(sourcemaps.init())// Generate and include map.
        .pipe(concat('templates/js/application.js'))
        .pipe(ngAnnotate())
        //.pipe(uglify()) // Minified line.
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'))
});

gulp.task('watch', ['app'], function () {
    gulp.watch('templates/js/app/**/*.js', ['app']);
})