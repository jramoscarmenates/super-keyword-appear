<?php
/**
 * Page to handle all Ajax requests for add data.
 * All data will be returned using JSON format
 *
 * General variables:
 *
 * @uses    $_REQUEST['object']            required
 */

/** Loads the WordPress Environment */
require_once( dirname( __FILE__ ) . '/../../../../../wp-load.php' );
// Query
if ( isset( $_REQUEST['action'] ) && 'query_structure' == $_REQUEST['action'] ) {
	//check_admin_referer( 'WPTelephoneDirectory-add-update-content' );

	$data_structure = WPTelephoneDirectory_Structure::get_structure();

	$message_to_return = _wptelephonedirectory__( 'The Structure was updated successfully.', 'wp_telephone_directory' );;
	$type_to_return = "success";
} else {
	$message_to_return = _wptelephonedirectory__( 'Error, Structure is null.', 'spirebuilder' );
	$type_to_return    = 'error';
}

$data['message'] = $message_to_return;
$data['type']    = $type_to_return;
$data['data']    = isset( $data_structure ) ? $data_structure : null;

wp_send_json($data);