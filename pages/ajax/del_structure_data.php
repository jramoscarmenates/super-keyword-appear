<?php
/**
 * Page to handle all Ajax requests for add data.
 * All data will be returned using JSON format
 *
 * General variables:
 *
 * @uses	$_REQUEST['object']			required
 */

/** Loads the WordPress Environment */
require_once( dirname(__FILE__) . '/../../../../../wp-load.php' );

// Adding
if ( isset($_REQUEST['action']) AND 'del' == $_REQUEST['action'] AND isset($_REQUEST['id']) AND '' != WPTelephoneDirectory_Validator::parse_string( $_REQUEST['id']) AND isset( $_REQUEST['submit_delete'] ) ) {
	//check_admin_referer('SuperContentInserter-add-update-content');

	if ( $data_to_delete = WPTelephoneDirectory_Structure::get(WPTelephoneDirectory_Validator::parse_int($_REQUEST['id']) ) ){
		if ( WPTelephoneDirectory_Structure::delete($data_to_delete->id) ){
			$message_to_return = _wptelephonedirectory__('Deleted successfully.' );
			$type_to_return = 'notification';
		} else {
			$message_to_return = _wptelephonedirectory__('Error to insert into database.' );
			$type_to_return = 'error';
		}
	} else {
		$message_to_return = _wptelephonedirectory__('Content block not found.' );
		$type_to_return = 'error';
	}
}

$data['message']	= $message_to_return;
$data['type'] 		= $type_to_return;

wp_send_json($data);