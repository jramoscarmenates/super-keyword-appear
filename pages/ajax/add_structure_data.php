<?php
/**
 * Page to handle all Ajax requests for add data.
 * All data will be returned using JSON format
 *
 * General variables:
 *
 * @uses	$_REQUEST['object']			required
 */

/** Loads the WordPress Environment */
require_once ( dirname(__FILE__) . '/../../../../../wp-load.php' );
// Adding
if ( isset($_REQUEST['action']) && 'add_structure' == $_REQUEST['action'] ) {
	if (  isset($_REQUEST['data']) && $_REQUEST['data'] != "" ) {
		//check_admin_referer( 'WPTelephoneDirectory-add-update-content' );

		$sanitized_data_structure = stripslashes($_REQUEST['data']);

		WPTelephoneDirectory_Structure::update_structure( json_decode($sanitized_data_structure));

		$message_to_return = _wptelephonedirectory__('The Structure was updated successfully.');
		$type_to_return = "success";
	} else {
		$message_to_return = _wptelephonedirectory__('Error, Structure is null.');
		$type_to_return    = 'error';
	}
}
$data_structure = WPTelephoneDirectory_Structure::get_structure();

$data['message']	= $message_to_return;
$data['type'] 		= $type_to_return;
$data['data'] =  isset($data_structure) ? $data_structure: Null;

wp_send_json($data);
