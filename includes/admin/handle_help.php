<?php
/**
 * Include to Help page.
 *
 * @package admin-panel
 *
 */

// Security: Check if is admin user
WPTelephoneDirectory_Users::an_admin_must_be_authenticated();

include( WPTelephoneDirectory::$template_dir . '/includes/admin/help.php');