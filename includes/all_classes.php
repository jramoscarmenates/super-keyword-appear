<?php
$plugin_dir = WPTelephoneDirectory::$plugin_dir;

include($plugin_dir . '/classes/dbobject.class.php');
include($plugin_dir . '/classes/structure.class.php');
include($plugin_dir . '/classes/people.class.php');
include($plugin_dir . '/classes/telephone.class.php');
include($plugin_dir . '/classes/users.class.php');
include($plugin_dir . '/classes/settings.class.php');
include($plugin_dir . '/classes/validator.class.php');