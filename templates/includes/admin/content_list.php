<?php
/**
 * Template to list all Structures.
 *
 */
?>
<div ng-app="wptd">

	<div class="cf-navbar navbar navbar-inverse" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#/">
					<?php _wptelephonedirectory_e( 'WP Directorio Telefónico' ); ?>
				</a>
			</div>

			<div class="collapse navbar-collapse">
				<button type="button" class="btn cf-btn cf-btn-create btn-success"
				        ng-click="openModal('lg', 'cf-half-modal', 'structure.add', 'structure.add', '', '')">
					<?php _wptelephonedirectory_e( 'Crear Estructura' ); ?>
				</button>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div ng-view></div>
		</div>
	</div>

	<!-- ================= Templates by Routes. ================= -->

	<!-- PARTIAL: Default Index page.
		================================================================================ -->
	<script id="structure.index" type="text/ng-template">

		<pre>
			{{ test | json }}
		</pre>

		<div class="col-sm-8 col-lg-8">
			<div ui-tree id="tree-root">
				<ol ui-tree-nodes="" ng-model="structures">
					<li ng-repeat="node in structures" ui-tree-node ng-include="'nodes_renderer.html'"></li>
				</ol>
			</div>
		</div>

		<div class="col-sm-4 col-lg-4">
			<pre>{{ data | json }}</pre>
		</div>

	</script>

	<!-- ================= Templates without Routes. ================= -->

	<!-- PARTIAL: Angular UI Tree.
		================================================================================ -->
	<script type="text/ng-template" id="nodes_renderer.html">
		<div ui-tree-handle class="tree-node tree-node-content">
			<a class="btn btn-success btn-xs" ng-if="node.nodes && node.nodes.length > 0" data-nodrag
			   ng-click="toggle(this)">
				<span class="glyphicon"
				      ng-class="{'glyphicon-chevron-right': collapsed, 'glyphicon-chevron-down': !collapsed}"></span>
			</a>

			{{node.name}}

			<a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="remove(this)">
				<span class="glyphicon glyphicon-remove"></span>
			</a>

			<a class="pull-right btn btn-primary btn-xs" data-nodrag ng-click="newSubItem(this)"
			   style="margin-right: 8px;"><span class="glyphicon glyphicon-plus"></span></a>
		</div>
		<ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
			<li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer.html'"></li>
		</ol>
	</script>

	<!-- PARTIAL: Generic modal to delete any element.
		================================================================================ -->
	<script id="delete_modal" type="text/ng-template">
		<div class="cf-delete-modal">
			<div class="modal-header">
				<button type="button" class="cf-close close fa fa-remove" ng-click="cancel()">×</button>
				<h5 class="modal-title">
					<?php _wptelephonedirectory_e( '¿Seguro que Desea Borrar el Elemento?' ); ?> -
					<strong>{{ title }}</strong>
				</h5>
			</div>
			<div ng-show="content" class="modal-body">
				{{ content }}
			</div>
			<div class="modal-footer">
				<button class="btn no-border btn-sm btn-danger" ng-click="delete()">
					<span class="ui-button-text">
						<i class="ace-icon fa fa-trash-o bigger-110"></i>&nbsp; Sí
					</span>
				</button>
				<button class="btn no-border btn-sm btn-default" ng-click="cancel()">
					<span class="ui-button-text">
						<i class="ace-icon fa fa-times bigger-110"></i>&nbsp; No
					</span>
				</button>
			</div>
		</div>
	</script>

	<!-- PARTIAL: Generic modal to add structure.
			================================================================================ -->
	<script id="structure.add" type="text/ng-template">
		<div class="cf-show-modal">
			<form name="form_structure_add" class="form-horizontal" role="form">
				<div class="modal-header">
					<button type="button" class="cf-close close fa fa-remove"
					        ng-click="modalInstance.dismiss('cancel')">
						×
					</button>
					<h5 class="modal-title">
						<?php _wptelephonedirectory_e( 'Crear Plantilla de Indicación' ); ?>:
						<strong>{{ structure.name }}</strong>
					</h5>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">
							<?php _wptelephonedirectory_e( 'Nombre' ); ?>
							<i class="glyphicon glyphicon-asterisk"></i>
						</label>

						<div class="col-sm-9">
							<input cffocus type="text" name="name" ng-model="structure.name" required
							       placeholder="<?php _wptelephonedirectory_e( 'Nombre de la Estructura' ); ?>"
							       class="col-xs-8 col-sm-8">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">
							<?php _wptelephonedirectory_e( 'Descripción' ); ?>
						</label>

						<div class="col-sm-9">
							<textarea name="description" ng-model="structure.description"
							          placeholder="<?php _wptelephonedirectory_e( 'Descripción de la Estructura' ); ?>"
							          class="col-xs-8 col-sm-8"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">
							<?php _wptelephonedirectory_e( 'Siglas' ); ?>
						</label>

						<div class="col-sm-9">
							<input type="text" name="acronym" ng-model="structure.acronym"
							       placeholder="<?php _wptelephonedirectory_e( 'Siglas de la Estructura' ); ?>"
							       class="col-xs-8 col-sm-8">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">
							<?php _wptelephonedirectory_e( 'Tipo' ); ?>
						</label>

						<div class="col-sm-9">
							<input type="text"
							       ng-model="structure.type"
							       typeahead="t for t in types | filter:$viewValue | limitTo:8"
							       placeholder="<?php _wptelephonedirectory_e( 'Seleccionar o Agregar Tipo de la Estructura' ); ?>"
							       class="col-xs-8 col-sm-8">
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn no-border btn-sm btn-primary" ng-disabled='!form_structure_add.$valid'
					        ng-click="create(structure)">
	                    <span class="ui-button-text">
	                        <i class="glyphicon glyphicon-edit"></i>&nbsp; <?php _wptelephonedirectory_e( 'Guardar' ); ?>
	                    </span>
					</button>
					<button class="btn no-border btn-sm btn-default" ng-click="modalInstance.dismiss('cancel')">
                    <span class="btn-default">
                        <i class="glyphicon glyphicon-remove"></i>&nbsp; <?php _wptelephonedirectory_e( 'Cancelar' ); ?>
                    </span>
					</button>
				</div>
			</form>
		</div>
	</script>
</div>