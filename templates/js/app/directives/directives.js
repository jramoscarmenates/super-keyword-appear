/**
 * Module Directives specific to app.
 */
'use strict';

angular.module("wptd.directives", [])

/**
 * Directive to edit inline extra elements.
 * Example to show ckeditor:
 *
 * <div cfeditable class="cfeditable">
 *      <div>[[ modelValue ]]</div> // This is the Show Area.
 *
 *      <div>                       // This is de Edit Area. TODO: VCA: Todo lo que esté aquí dentro es lo que se muestra al dar clik sobre el elemento.
 *          <div ng-cloak ng-hide="isReady" class="highlight">
 *              Initialising ...
 *          </div>
 *
 *          <textarea ckeditor="editorOptions" ng-model="modelValue"></textarea>
 *      </div>
 * </div>
 *
 * The directive accepts the following attributes:
 *
 * - 'buttons': Default value is 'no'. Boolean 'yes/no' to show or not the close button.
 * - 'autoclose': Default value is 'outside'. String 'no/auto/outside' to close Edit Area.
 */
    .
    directive('cfeditable', ['$document', '$parse', function ($document, $parse) {
        return {
            restrict: 'A',
            scope   : {},
            link    : function (scope, element, attrs) {
                // Options.
                var buttons = (!!attrs.buttons) ? attrs.buttons : 'no';
                var autoClose = (!!attrs.autoclose) ? attrs.autoclose : 'outside';

                var $this = element.find('div');

                var $show;
                var $edit;

                $show = $this.eq(0);
                $edit = $this.eq(1);

                $edit.hide();

                var show = function () {
                    $show.show();
                    $edit.hide();
                };

                var edit = function () {
                    $show.hide();
                    $edit.show();
                };

                // TODO: VCA: si buttons existe y es no entonces no se muestra los botones automáticos porque el usuario pone los suyos propios
                if (buttons && buttons == 'yes') {
                    var editableButtons = '' +
                        '<div class="editable-buttons">' +
                            /*'<button type="submit" class="btn btn-info editable-submit">' +
                             '<i class="ace-icon fa fa-check"></i>' +
                             '</button>' +*/

                        '<button type="button" class="btn editable-cancel">' +
                        '<i class="ace-icon fa fa-times"></i>' +
                        '</button>' +
                        '</div>';
                    $edit.append(editableButtons);
                }

                // When click in element show then hide this and show edit area of element.
                $show.bind('click', function (e) {
                    edit();
                    e.stopPropagation();
                });

                if (autoClose) {

                    // Close edit area when mouse leave.
                    if (autoClose == 'auto') {
                        $edit.bind('mouseleave', function () {
                            show();
                        });
                    }

                    // Close edit area when click outside.
                    if (autoClose == 'outside') {
                        $edit.bind('click', function (e) {
                            e.stopPropagation();
                        });

                        $document.on('click', function () {
                            show();
                        });
                    }
                }

                var $btnClose = element.find('.editable-cancel');

                // Close edit area when click in Close button.
                $btnClose.bind('click', function () {
                    show();
                });
            }
        }
    }])

/**
 * Directive to complemented tables with extra directive si-table.
 * Example to show table:
 *
 * <div cftable fnc="getData()">
 *      <table si-table sorting="params.sortBy" class="table table-responsive table-bordered cf-table">
 *          ...
 *      </table>
 * </div>
 *
 * The directive accepts the following attributes:
 *
 * - 'fnc': Default value is 'getData()'. Function to execute to get data for show in table.
 */
    .directive('cftable', ['$document', '$parse', '$rootScope', function ($document, $parse, $rootScope) {
        return {
            restrict   : 'A',
            replace    : true,
            transclude : true,
            templateUrl: 'bundles/cfsclinic/js/app/partials/table.html',
            controller : ['$scope', '$attrs', function ($scope, $attrs) {

                var executeFunction = (!!$attrs.fnc) ? $parse($attrs.fnc) : $parse('getData()');

                // Start Block to execute table.
                $scope.params = {
                    offset: 0,
                    limit : $rootScope.tableParams.limit || 10
                };

                $scope.search = '';
                $scope.limits = $rootScope.tableParams.limits || [10, 25, 50, 100];

                $scope.init = true;
                function getData() {
                    executeFunction($scope);
                }

                function watch() {
                    if ($scope.init == false) {
                        getData();
                    }
                }

                $scope.$watch('params.offset', getData);
                // TODO: VCA: Al descomentar esta linea se podra ordenar (sort by) por los nombres de la tabla.
                //$scope.$watch('params.sortBy', watch, true);
                $scope.$watch('params.limit', watch, true);
                //$scope.$watch('search', getData);

                // Search function.
                $scope.shortSearch = function (search) {
                    if (search && search.length > 2) {
                        $scope.search = search;
                        getData();
                    }
                };

                $scope.closeSearch = function () {
                    $scope.search = null;
                    getData();
                };

                // Angularjs sockets.
                $rootScope.$on('refresh:table', function () {
                    getData();
                });
                // End Block to execute table.

            }]
        }
    }])

/**
 * Directive to focus a element.
 * Example:
 *
 * <input cffocus type="text" name="name" ng-model="indicationAdd.name" required>
 */
    .directive('cffocus', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope   : {},
            link    : function (scope, element) {
                var $this = element;

                $timeout(function () {
                    $this.focus();
                }, 1000);
            }
        }
    }])