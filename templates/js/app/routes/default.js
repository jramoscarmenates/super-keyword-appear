'use strict';

angular.module("default.routes", [])

    .config(['$routeProvider', function ($routeProvider) {

        /**
         * Routes.
         */

        $routeProvider

            .when('/', {
                templateUrl: 'structure.index',
                controller : 'structure.index'
            })
    }])