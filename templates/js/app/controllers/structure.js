/**
 * Module Controller to generic page.
 */
'use strict';

angular.module("wptd.structure", ['default.routes'])

/**
 * Controllers by Routes.
 */

    .controller('structure.index', ['$scope', '$rootScope', '$location', 'structureModel', function ($scope, $rootScope, $location, structureModel) {
        structureModel.query(function (result) {
            if (result) {
                $scope.test = result;
                if (!!result.data && typeof result.data == 'object') {
                    $rootScope.structures = result.data;
                }
            }
        });
    }])

/**
 * Create structure.
 */
    .controller('structure.add', ['$scope', '$rootScope', '$location', '$modalInstance', 'structureModel', function ($scope, $rootScope, $location, $modalInstance, structureModel) {
        $scope.modalInstance = $modalInstance;

        $scope.structure = {
            type: ''
        };
        $scope.types = [];

        $scope.create = function (structure) {
            if (structure) {
                var tmpStructures = $rootScope.structures;
                tmpStructures.push(structure);

                structureModel.add(tmpStructures, function (result) {
                    $rootScope.structures = result.data;
                    //flashMessageLaunch(result);
                });
            }

            $modalInstance.close();
        }
    }])