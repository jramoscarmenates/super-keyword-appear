'use strict';

angular.module("wptd.model", [])

    .factory('structureModel', ['$http', '$rootScope', function ($http, $rootScope) {
        return {

            /**
             * Add structure.
             *
             * @param json
             * @param callback
             */
            add: function (data, callback) {
                $http.post(ajaxurl, '', {
                    params: {
                        data  : angular.toJson(data),
                        action: 'add_structure'
                    }
                }).success(function (result, status) {
                    if (status == 200) {
                        if (result) {

                            callback(result);
                        }
                        else {
                            flashMessageLaunch(result);
                        }
                    }
                    else {
                        flashMessageLaunch({type: 'error', msg: 'Error: Please refresh page.'});
                    }
                });
            },

            /**
             * Get all structures.
             *
             * @param callback
             */
            query: function (callback) {
                $http.post(ajaxurl, '', {
                    params: {
                        action: 'query_structure'
                    }
                }).success(function (result, status) {
                    if (status == 200) {
                        if (result) {

                            callback(result);
                        }
                        else {
                            flashMessageLaunch(result);
                        }
                    }
                    else {
                        flashMessageLaunch({type: 'error', msg: 'Error: Please refresh page.'});
                    }
                });
            }
        }
    }])