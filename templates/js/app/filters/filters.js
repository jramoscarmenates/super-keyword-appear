/**
 * Module Filters specific to app.
 */
'use strict';

angular.module("wptd.filters", [])

/**
 * Parse content to html.
 */
    .filter('cfhtml', ['$sce', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    }])