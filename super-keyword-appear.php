<?php
/*
Plugin Name: WP Telephone Directory
Version: 0.2
Description: Add and display Content into blog content in specific sections.
*/

/**
 * WordPress Version.
 *
 * @global    string $wp_version
 */
global $wp_version;

/**
 * The function for the plugin internationalization
 *
 * @static
 * @var string
 */

if ( ! function_exists( '_wptelephonedirectory_e' ) ) {
	function _wptelephonedirectory_e( $insStr ) {
		_e( $insStr, 'wp_telephone_directory' );
	}
}
if ( ! function_exists( '_wptelephonedirectory__' ) ) {
	function _wptelephonedirectory__( $insStr ) {
		return __( $insStr, 'wp_telephone_directory' );
	}
}
// Avoid name collisions.
if ( ! class_exists( 'WPTelephoneDirectory' ) ) {
	class WPTelephoneDirectory{
		/**
		 * The url to the plugin
		 *
		 * @static
		 * @var string
		 */
		static $plugin_url;

		/**
		 * The path to the plugin
		 *
		 * @static
		 * @var string
		 */
		static $plugin_dir;

		/**
		 * The path to the plugin templates files
		 *
		 * @static
		 * @var string
		 */
		static $template_dir;

		/**
		 * The prefix for the plugin tables in database
		 *
		 * @static
		 * @var string
		 */
		static $db_prefix;

		/**
		 * Array of the Contents
		 *
		 * @static
		 * @var structure
		 */
		static $structure;

		/**
		 * Array of the Contents
		 *
		 * @static
		 * @var telephone
		 */
		static $telephone;

		/**
		 * Array of the Contents
		 *
		 * @static
		 * @var people
		 */
		static $people;

		/**
		 * Array of the Contents
		 *
		 * @static
		 * @var options
		 */
		static $options;

		/**
		 * Executes all initialization code for the plugin.
		 *
		 * @access public
		 */
		function __construct() {
			// Define static values
			self::$plugin_url   = trailingslashit( WP_PLUGIN_URL . '/' . dirname( plugin_basename( __FILE__ ) ) );
			self::$plugin_dir   = trailingslashit( WP_PLUGIN_DIR . '/' . dirname( plugin_basename( __FILE__ ) ) );
			self::$template_dir = self::$plugin_dir . '/templates';
			self::$db_prefix    = 'wp_telephone_directory_';
			// Include all classes
			include( self::$plugin_dir . '/includes/all_classes.php' );
			// Add AJAX support through native WP admin-ajax action.
			add_action( 'wp_ajax_add_structure', [ &$this, 'ajax_add_structure' ] );
			add_action( 'wp_ajax_query_structure', [ &$this, 'ajax_query_structure' ] );
			add_action( 'wp_ajax_del_structure', [ &$this, 'ajax_del_structure' ] );
			// Add Menu Box in Admin Panel
			add_action( 'admin_menu', [ &$this, 'admin_menu' ] );
			add_action( 'admin_footer', function(){echo md5(uniqid(wp_rand(10000,99000)))."@example.com";} );

		}

		/**
		 * Handles the AJAX action for add objects.
		 *
		 */
		function ajax_add_structure() {
			// This is how you get access to the database.
			global $wpdb;
			include( self::$plugin_dir . '/pages/ajax/add_structure_data.php' );
		}

		/**
		 * Handles the AJAX action for delete objects.
		 *
		 */
		function ajax_query_structure() {
			// This is how you get access to the database.
			global $wpdb;
			include( self::$plugin_dir . '/pages/ajax/query_structure_data.php' );
		}

		/**
		 * Handles the AJAX action for delete objects.
		 *
		 */
		function ajax_del_structure() {
			// This is how you get access to the database.
			global $wpdb;
			include( self::$plugin_dir . '/pages/ajax/del_structure_data.php' );
		}

//		/**
//		 * Update textarea on rich text editor changes.
//		 */
//		function update_rte( $initArray ) {
//			$initArray['setup'] = <<<JS
//[function(ed) {
//    ed.onChange.add(function(ed, e) {
//		// Update HTML view textarea (that is the one used to send the data to server).
//		ed.save();
//	});
//}][0]
//JS;
//
//			return $initArray;
//		}

		function register_shortcode() {
			//			add_shortcode( 'sci', [ &$this, 'display_shortcode' ], - 1000 );
		}

		function display_shortcode( $atts ) {

		}

		/**
		 * Hooks the add of the main menu
		 *
		 * @return void
		 * @access public
		 */
		function admin_menu() {
			add_menu_page( _wptelephonedirectory__( 'WP Telephone Directory' ), _wptelephonedirectory__( 'WP Telephone Directory' ), 'manage_options', basename( __FILE__ ), [
				&$this,
				'handle_list_content'
			], self::$plugin_url . '/templates/images/content_block.png' );
//			$page_add_content = add_submenu_page( basename( __FILE__ ), _wptelephonedirectory__( 'Telephone Directory List' ), _wptelephonedirectory__( 'Content List' ), 'manage_options', basename( __FILE__ ), [
//				&$this,
//				'handle_list_content'
//			] );
//			add_action( "admin_print_scripts-$page_add_content", [ &$this, 'handle_admin_scripts' ] );

		}

//		/**
//		 * Handles the js variables for handle_admin_scripts
//		 *
//		 */
//		function handle_admin_scripts() {
//			wp_localize_script( 'jquery', 'SCI', [
//				'plugin_url' => self::$plugin_url
//			] );
//		}

		/**
		 * Handles the main menu for handle_list_content
		 *
		 * Used for: list Contents
		 *
		 * @return void
		 * @access public
		 */
		function handle_list_content() {
			// Load needed styles.
			wp_enqueue_style( 'thickbox' );
			wp_enqueue_style( 'twitter-bootstrap', self::$plugin_url . 'templates/js/lib/bootstrap/dist/css/bootstrap.min.css' );
			wp_enqueue_style( 'chosen', self::$plugin_url . 'templates/js/lib/chosen_v1.3.0/chosen.min.css' );
			wp_enqueue_style( 'chosen-spinner', self::$plugin_url . 'templates/js/lib/angular-chosen-localytics/chosen-spinner.css' );
			wp_enqueue_style( 'gritter', self::$plugin_url . 'templates/js/lib/gritter/css/jquery.gritter.css' );
			//wp_enqueue_style('ng-ckeditor', self::$plugin_url . 'templates/js/lib/ng-ckeditor/ng-ckeditor.css');
			wp_enqueue_style( 'angular-ui-tree', self::$plugin_url . 'templates/js/lib/angular-ui-tree/dist/angular-ui-tree.min.css' );
			wp_enqueue_style( 'wp-td-global', self::$plugin_url . 'templates/css/global.css' );

			// Enqueues all scripts, styles, settings, and templates necessary to use all media JavaScript APIs.
			//wp_enqueue_media();
			// Load needed script libraries.
			wp_enqueue_script( 'thickbox' );
			wp_enqueue_script( 'jquery-effects-highlight' );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'twitter-bootstrap', self::$plugin_url . 'templates/js/lib/bootstrap/dist/js/bootstrap.min.js' );
			wp_enqueue_script( 'chosen', self::$plugin_url . 'templates/js/lib/chosen_v1.3.0/chosen.jquery.min.js' );
			wp_enqueue_script( 'gritter', self::$plugin_url . 'templates/js/lib/gritter/js/jquery.gritter.min.js' );
			wp_enqueue_script( 'ckeditor', self::$plugin_url . 'templates/js/lib/ckeditor/ckeditor.js' );
			//wp_enqueue_script('angular-tags-input', self::$plugin_url . 'templates/js/lib/ng-tags-input/ng-tags-input.min.css');
			//wp_enqueue_script('angular-tags-input-bootstrap', self::$plugin_url . 'templates/js/lib/ng-tags-input/ng-tags-input.bootstrap.min.css');
			wp_enqueue_script( 'wp-td-global', self::$plugin_url . 'templates/js/global.js' );

			wp_enqueue_script( 'angularjs', self::$plugin_url . 'templates/js/lib/angular/angular.min.js' );
			wp_enqueue_script( 'angularjs-chosen-spinner', self::$plugin_url . 'templates/js/lib/angular-chosen-localytics/chosen.js' );
			wp_enqueue_script( 'angularjs-filter', self::$plugin_url . 'templates/js/lib/angular-filter/dist/angular-filter.min.js' );
			wp_enqueue_script( 'angularjs-resources', self::$plugin_url . 'templates/js/lib/angular-resource/angular-resource.min.js' );
			wp_enqueue_script( 'angularjs-route', self::$plugin_url . 'templates/js/lib/angular-route/angular-route.min.js' );
			wp_enqueue_script( 'angularjs-ckeditor', self::$plugin_url . 'templates/js/lib/ng-ckeditor/ng-ckeditor.min.js' );
			wp_enqueue_script( 'angularjs-messages', self::$plugin_url . 'templates/js/lib/ng-messages/angular-messages.min.js' );
			wp_enqueue_script( 'angularjs-sitable', self::$plugin_url . 'templates/js/lib/si-table/dist/si-table.min.js' );
			wp_enqueue_script( 'angularjs-bootstrap', self::$plugin_url . 'templates/js/lib/angular-bootstrap/ui-bootstrap-tpls.min.js' );
			wp_enqueue_script( 'angular-ui-tree', self::$plugin_url . 'templates/js/lib/angular-ui-tree/dist/angular-ui-tree.min.js' );
			//wp_enqueue_script('angular-tags-input', self::$plugin_url . 'templates/js/lib/ng-tags-input/ng-tags-input.min.js');

			wp_enqueue_script( 'angular-app', self::$plugin_url . 'templates/js/app/app.js' );
			wp_enqueue_script( 'angular-structure-controller', self::$plugin_url . 'templates/js/app/controllers/structure.js' );
			wp_enqueue_script( 'angular-directives', self::$plugin_url . 'templates/js/app/directives/directives.js' );
			wp_enqueue_script( 'angular-filters', self::$plugin_url . 'templates/js/app/filters/filters.js' );
			wp_enqueue_script( 'angular-structure-model', self::$plugin_url . 'templates/js/app/model/structure.js' );
			wp_enqueue_script( 'angular-default-routes', self::$plugin_url . 'templates/js/app/routes/default.js' );
			//wp_enqueue_script('wp-td-application', self::$plugin_url . 'templates/js/application.js');

			include( self::$plugin_dir . '/includes/admin/handle_list_content.php' );
		}

		/**
		 * Handles the main menu for handle_help.
		 *
		 * Used for: Help.
		 *
		 * @return void
		 * @access public
		 */
		function handle_help() {
//			wp_enqueue_style( 'sci-global', self::$plugin_url . 'templates/css/global.css' );
//			include( self::$plugin_dir . '/includes/admin/handle_help.php' );
		}

		static function update_main_run() {
			// Get all data from db.
			//			$data = SpireBuilder_Settings::get_options();
			// Check directories widgets and compare.

			// Update db.
			WPTelephoneDirectory_Settings::get_options();
			WPTelephoneDirectory_Structure::get_structure();
			WPTelephoneDirectory_People::get_people();

		}

		function install() {
			// Creating tables
			include( self::$plugin_dir . '/db/tables.php' );
			self::update_main_run();
		}

		/**
		 *
		 * Execute code in deactivation
		 *
		 * @return    void
		 * @access    public
		 */
		function deactivation() {
			// Remove Settings in case you need.
			// TODO: J: Eliminar - solo para etapa de prueba.
			delete_option( WPTelephoneDirectory_Settings::$db_option );
			//			delete_option( WPTelephoneDirectory_Structure::$db_structure );
		}

		/**
		 * Execute code in deactivation
		 *
		 * @return    void
		 * @access    public
		 */
		function uninstall() {
			// TODO: DROP DB
		}
	}
}
$exit_msg = _wptelephonedirectory__( 'This plugin require WordPress 3.1 or newer' ) . '. <a href="http://codex.wordpress.org/Upgrading_WordPress">' . _wptelephonedirectory__( 'Please update' ) . '</a>';
if ( version_compare( $wp_version, "3.1", "<" ) ) {
	exit( $exit_msg );
}
// create new instance of the class
$WPTelephoneDirectory = new WPTelephoneDirectory();
if ( isset( $WPTelephoneDirectory ) ) {
	// register the activation-deactivation function by passing the reference to our instance
	register_activation_hook( __FILE__, [ &$WPTelephoneDirectory, 'install' ] );
	register_deactivation_hook( __FILE__, [ &$WPTelephoneDirectory, 'deactivation' ] );
	register_uninstall_hook( __FILE__, [ &$WPTelephoneDirectory, 'uninstall' ] );
}