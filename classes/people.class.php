<?php
if ( ! class_exists( 'WPTelephoneDirectory_People' ) ) {
	/**
	 * Class WPTelephoneDirectory_People
	 *
	 * All Structure the WP Telephone People
	 *
	 */
	class WPTelephoneDirectory_People{
		/**
		 * The name for plugin people in the DB
		 *
		 * @var string
		 */
		static $db_people = 'WPTelephoneDirectory_People';

		/**
		 * Get setting value: global allow_spire_builder_editor
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_builder_content_types() {
			$people = self::get_people();

			return $people['settings']['builder_content_types'];
		}

		/**
		 * Updates the General Settings of Plugin
		 *
		 * @param array $people
		 *
		 * @return array
		 * @access public
		 */
		static public function update_people( $people ) {
			// Save Class variable
			WPTelephoneDirectory::$people = $people;

			return update_option( self::$db_people, $people );
		}

		/**
		 * Return the General Settings of Plugin, and set them to default values if they are empty
		 *
		 * @return array general structure of plugin
		 * @access public
		 */
		static function get_people() {
			// If isn't empty, return class variable
			if ( WPTelephoneDirectory::$people ) {
				return WPTelephoneDirectory::$people;
			}

			// Save class variable
			WPTelephoneDirectory::$people = get_option( self::$db_people );

			//return the structure
			return WPTelephoneDirectory::$people;
		}
	}
}
