<?php
if (!class_exists('WPTelephoneDirectory_Structure')) {
	/**
	 * Class WPTelephoneDirectory_Structure
	 *
	 * All Structure the WP Telephone Directory
	 *
	 */
	class   WPTelephoneDirectory_Structure {
		/**
		 * The name for plugin structure in the DB
		 *
		 * @var string
		 */
		static $db_structure = 'WPTelephoneDirectory_Structure';

		/**
		 * Get setting value: global allow_spire_builder_editor
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_builder_content_types() {
			$structure = self::get_structure();

			return $structure['settings']['builder_content_types'];
		}

		/**
		 * Updates the General Settings of Plugin
		 *
		 * @param array $structure
		 *
		 * @return array
		 * @access public
		 */
		static public function update_structure($structure) {
			// Save Class variable
			WPTelephoneDirectory::$structure = $structure;

			return update_option(self::$db_structure, $structure);
		}

		/**
		 * Return the General Settings of Plugin, and set them to default values if they are empty
		 *
		 * @return array general structure of plugin
		 * @access public
		 */
		static function get_structure() {
			// If isn't empty, return class variable
			if (WPTelephoneDirectory::$structure) {
				return WPTelephoneDirectory::$structure;
			}

			// Save class variable
			WPTelephoneDirectory::$structure = get_option(self::$db_structure);

			//return the structure
			return WPTelephoneDirectory::$structure;
		}
	}
}
