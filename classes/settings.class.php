<?php
if (!class_exists('WPTelephoneDirectory_Settings')) {
	/**
	 * Class WPTelephoneDirectory_Settings
	 *
	 * All Settings the WP Telephone Directory
	 *
	 */
	class WPTelephoneDirectory_Settings {
		/**
		 * The name for plugin options in the DB
		 *
		 * @var string
		 */
		static $db_option = 'WPTelephoneDirectory_Options';

		/**
		 * Get setting value: global allow_spire_builder_editor
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_allow_spire_builder_editor() {
			$options = self::get_options();

			return $options['settings']['allow_spire_builder_editor'];
		}

		/**
		 * Get setting value: global allow_spire_builder_editor
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_network_admin_only() {
			$options = self::get_options();

			return $options['settings']['network_admin_only'];
		}

		/**
		 * Get setting values
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_settings() {
			$options = self::get_options();

			return $options['settings'];
		}

		/**
		 * Get setting values
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_requirements() {
			$options = self::get_options();

			return $options['requirements'];
		}

		/**
		 * Get setting value: locale
		 *
		 * @static
		 * @return bool
		 * @access public
		 */
		static public function get_locale() {
			$options = self::get_options();

			return $options['locale'];
		}

		/**
		 * Updates the General Settings of Plugin
		 *
		 * @param array $options
		 *
		 * @return array
		 * @access public
		 */
		static public function update_options($options) {
			// Save Class variable
			SpireBuilder::$options = $options;

			return update_option(self::$db_option, $options);
		}

		/**
		 * Return the General Settings of Plugin, and set them to default values if they are empty
		 *
		 * @return array general options of plugin
		 * @access public
		 */
		static function get_options() {
			// If isn't empty, return class variable
			if (WPTelephoneDirectory::$options) {
				return WPTelephoneDirectory::$options;
			}

			// default values
			$options = array( // Global Settings
				'settings' => array('network_admin_only' => 1,
				                    'desactivation_delete_data' => '1' /* if the plugins is disabled  */,), // end settings
				// Requirements
				'requirements' => array('wp_version' => '3.5.2', 'php_version_min' => '5.2.9', 'php_version_tested' => '5.4.1'), // end requirements
				'locale' => '', // '' is default => English
				'templates' => array(), //
			);

			// get saved options
			$saved = get_option(self::$db_option);

			// assign them
			if (!empty($saved)) {
				foreach ($saved as $key => $option) {
					$options[$key] = $option;
				}
			}

			// update the options if necessary
			if ($saved != $options) {
				update_option(self::$db_option, $options);
			}

			// Save class variable
			WPTelephoneDirectory::$options = $options;

			//return the options
			return $options;
		}
	}
}
