<?php
if ( ! class_exists( 'WPTelephoneDirectory_Telephone' ) ) {
	/**
	 * Class WPTelephoneDirectory_Telephone
	 *
	 * All Structure the WP Telephone
	 *
	 */
	class WPTelephoneDirectory_Telephone{
		/**
		 * The name for plugin telephone in the DB
		 *
		 * @var string
		 */
		static $db_telephone = 'WPTelephoneDirectory_Telephone';
		
		/**
		 * Get setting value: global allow_spire_builder_editor
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_builder_content_types() {
			$telephone = self::get_telephone();
			
			return $telephone['settings']['builder_content_types'];
		}
		
		/**
		 * Updates the General Settings of Plugin
		 *
		 * @param array $telephone
		 *
		 * @return array
		 * @access public
		 */
		static public function update_telephone( $telephone ) {
			// Save Class variable
			WPTelephoneDirectory::$telephone = $telephone;
			
			return update_option( self::$db_telephone, $telephone );
		}
		
		/**
		 * Return the General Settings of Plugin, and set them to default values if they are empty
		 *
		 * @return array general structure of plugin
		 * @access public
		 */
		static function get_telephone() {
			// If isn't empty, return class variable
			if ( WPTelephoneDirectory::$telephone ) {
				return WPTelephoneDirectory::$telephone;
			}
			
			// Save class variable
			WPTelephoneDirectory::$telephone = get_option( self::$db_telephone );
			
			//return the structure
			return WPTelephoneDirectory::$telephone;
		}
	}
}
